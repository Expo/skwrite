import { writable } from 'svelte/store';
import { AppwriteException, ID } from 'appwrite';
import { goto } from '$app/navigation';
import { account } from '$lib/appwrite';
import type { Models } from 'appwrite';

const store = writable<Models.User<Models.Preferences> | null>(null);

let initPromise: Promise<false> | undefined = undefined;

const init = async () => {
  initPromise =
    initPromise ??
    (async () => {
      try {
        store.set(await account.get());
      } catch (e) {
        if (e instanceof AppwriteException) {
          const r = e.response as unknown;
          if (
            r instanceof Object &&
            'message' in r &&
            typeof r.message === 'string' &&
            r.message.includes('missing scope')
          ) {
            console.info(
              'Encountered Auth Failure:',
              r.message,
              '\nThe user is likely not logged in.',
            );
          } else {
            console.warn('Encountered Auth Failure:', e);
          }
        } else console.warn('Encountered Auth Failure', e);

        store.set(null);
      }
      return false as const;
    })();
  await initPromise;
};
const update = async () => {
  await initPromise;
  initPromise = undefined;
  await init();
};

const register = async (email: string, password: string) => {
  await account.create(ID.unique(), email, password);
  await login(email, password);
};

const login = async (email: string, password: string) => {
  await account.createEmailSession(email, password);
  await init();
  goto('/');
};

const logout = async () => {
  await account.deleteSession('current');
  store.set(null);
};

// Expose the store's value with $user
export const user = {
  subscribe: store.subscribe,
  register,
  login,
  logout,
  init,
  update,
};
export default user;
