import { dev } from '$app/environment';
import { Client, Databases, Account, Functions } from 'appwrite';

export const client = new Client();

// Change this:
client
  .setEndpoint(dev ? 'http://localhost/v1' : 'https://example.com/v1')
  .setProject('apptest');

export const funcs = new Functions(client);
export const account = new Account(client);
export const databases = new Databases(client);
