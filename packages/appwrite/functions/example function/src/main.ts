/**
 * @id example-function
 * @name Example Function
 * @description An example function
 */

import 'source-map-support/register';

// Load .env
import dotenv from 'dotenv';
dotenv.config({
  path: __dirname + '/../.env',
});

// Function Contents
import { Client } from 'node-appwrite';
import App from '@written/app';
import { z } from 'zod';

const app = new App().cors();

const env = z
  .object({
    APPWRITE_LOCATION: z.string(),
    APPWRITE_FUNCTION_ENDPOINT: z.string(),
    APPWRITE_FUNCTION_PROJECT_ID: z.string(),
    APPWRITE_API_KEY: z.string(),
    NODE_ENV: z.string().default('production'),
  })
  .parse(process.env);

const client = new Client()
  .setEndpoint(env.APPWRITE_LOCATION)
  .setProject(env.APPWRITE_FUNCTION_PROJECT_ID)
  .setKey(env.APPWRITE_API_KEY);

app.get(null, async ({ req, res, log, error }) => {
  // null = any route
  log('Hello, Logs!');
  error('Hello, Errors!');
  return res.send('Hello, World!');
});
app.use(({ res }) => {
  // `res.json()` is a handy helper for sending JSON
  return res.json({
    motto: 'Build like a team of hundreds_',
    learn: 'https://appwrite.io/docs',
    connect: 'https://appwrite.io/discord',
    getInspired: 'https://builtwith.appwrite.io',
  });
});

export default app.server();
