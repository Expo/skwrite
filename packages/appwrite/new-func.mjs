#!/usr/bin/env node
// @ts-check

import fs from 'fs';
import prompts from 'prompts';

import { dirname } from 'path';
import { fileURLToPath } from 'url';

const __dirname = dirname(fileURLToPath(import.meta.url));

(async (packageJson, gitignore) => {
  const appwriteFilePath = __dirname + '/appwrite.json';
  const appwriteFile = JSON.parse(fs.readFileSync(appwriteFilePath, 'utf-8'));
  const { id, funcName, description } = await prompts(
    [
      {
        name: 'funcName',
        message: 'Function Human-readable Name:',
        type: 'text',
        validate: v => (v.length === 0 ? 'Please enter a value.' : true),
      },
      {
        name: 'id',
        message: 'Function ID:',
        type: 'text',
        validate: v =>
          v.length === 0
            ? 'Please enter a value.'
            : v.toLowerCase() === v
            ? v.match(/^[a-z0-9-]*$/giu)
              ? true
              : 'Must only contain a-z, 0-9 and hyphens.'
            : 'Must be lowercase!',
        initial: n =>
          n
            .toLowerCase()
            .trim()
            .replace(/[^a-zA-Z0-9_]/giu, '-'),
      },
      {
        name: 'description',
        message: 'Description:',
        type: 'text',
        initial: packageJson.description,
      },
    ],
    {
      onCancel: () => process.exit(129),
    },
  );
  const safeDisplayName = funcName.replace(/[^a-zA-Z0-9 _-]/giu, '-');
  const relativeDir = 'functions/' + safeDisplayName.toLowerCase();
  packageJson.name = id;
  packageJson.description = description;
  packageJson.displayName = funcName;
  packageJson.scripts[
    'appwrite-deploy'
  ] = `npm run build && cd ../.. && appwrite deploy function --functionId ${JSON.stringify(
    id,
  )} && cd ${JSON.stringify(relativeDir)}`;
  appwriteFile.functions = appwriteFile.functions ?? [];
  appwriteFile.functions.push({
    $id: id,
    name: funcName,
    runtime: 'node-20.0',
    execute: ['any'],
    schedule: '',
    timeout: 15,
    enabled: true,
    logging: true,
    entrypoint: packageJson.main,
    commands: `npm install --omit=dev;
npm run appwrite:build --if-present;
`,
    ignore: ['node_modules', '.npm'],
    path: relativeDir,
  });
  const dir = __dirname + '/' + relativeDir + '/';
  const srcDir = dir + 'src/';
  if (!fs.existsSync(dir)) fs.mkdirSync(dir);
  if (!fs.existsSync(srcDir)) fs.mkdirSync(srcDir);
  fs.writeFileSync(dir + 'package.json', JSON.stringify(packageJson, null, 2));
  fs.writeFileSync(dir + '.gitignore', gitignore);
  fs.writeFileSync(
    dir + 'README.md',
    `# ${funcName}

${description}

## 🧰 Usage

### GET /

- Returns a "Hello, World!" message.

**Response**

Sample \`200\` Response:

\`\`\`text
Hello, World!
\`\`\`

### POST, PUT, PATCH, DELETE /

- Returns a "Learn More" JSON response.

**Response**

Sample \`200\` Response:

\`\`\`json
{
  "motto": "Build like a team of hundreds_",
  "learn": "https://appwrite.io/docs",
  "connect": "https://appwrite.io/discord",
  "getInspired": "https://builtwith.appwrite.io"
}
\`\`\`

## ⚙️ Configuration

| Setting           | Value         |
|-------------------|---------------|
| Runtime           | Node (20.0)   |
| Entrypoint        | \`src/main.js\` |
| Build Commands    | \`npm install\` |
| Permissions       | \`any\`         |
| Timeout (Seconds) | 15            |

## 🔒 Environment Variables

No environment variables required.
`,
  );
  fs.writeFileSync(
    srcDir + 'main.ts',
    `/**
 * @id ${id}
 * @name ${funcName}
 * @description ${description.split('\n').join(`
 * `)}
 */

import 'source-map-support/register';

// Load .env
import dotenv from 'dotenv';
dotenv.config({
  path: __dirname + '/../.env',
});

// Function Contents
import { Client } from 'node-appwrite';
import App from '@written/app';
import { z } from 'zod';

const app = new App().cors();

const env = z
  .object({
    APPWRITE_LOCATION: z.string(),
    APPWRITE_FUNCTION_ENDPOINT: z.string(),
    APPWRITE_FUNCTION_PROJECT_ID: z.string(),
    APPWRITE_API_KEY: z.string(),
    NODE_ENV: z.string().default('production'),
  })
  .parse(process.env);

const client = new Client()
  .setEndpoint(env.APPWRITE_LOCATION)
  .setProject(env.APPWRITE_FUNCTION_PROJECT_ID)
  .setKey(env.APPWRITE_API_KEY);
  
app.get(null, async ({ req, res, log, error }) => { // null = any route
  log('Hello, Logs!');
  error('Hello, Errors!');
  return res.send('Hello, World!');
});
app.use(({res}) => {
  // \`res.json()\` is a handy helper for sending JSON
  return res.json({
    motto: 'Build like a team of hundreds_',
    learn: 'https://appwrite.io/docs',
    connect: 'https://appwrite.io/discord',
    getInspired: 'https://builtwith.appwrite.io',
  });
});

export default app.server();
`,
  );
  fs.writeFileSync(appwriteFilePath, JSON.stringify(appwriteFile, null, 2));
  fs.writeFileSync(
    dir + '/.env',
    `# Your Environment Vars go here
# Project-Wide Variables should be uploaded to https://<appwrite>/console/project-rbux/settings  
`,
  );
  fs.writeFileSync(
    dir + '/project.json',
    JSON.stringify(
      {
        targets: {
          build: {
            dependsOn: [
              {
                projects: ['appwrite'],
                target: 'build',
                params: 'ignore',
              },
              {
                dependencies: true,
                target: 'build',
                params: 'ignore',
              },
            ],
          },
        },
      },
      null,
      2,
    ),
  );
  fs.copyFileSync(dir + '/.env', dir + '/.env.example');
})(
  {
    name: 'base-func',
    displayName: 'Base Func',
    version: '1.0.0',
    description: 'A base function',
    main: 'dist/main.js',
    source: 'src/main.ts',
    type: 'commonjs',
    scripts: {
      build:
        'esbuild --bundle --format=cjs --platform=node --outfile=dist/main.js --sourcemap=inline src/main.ts',
      format: 'prettier -c -w .',
      dev: 'nodemon --watch . -e ts,json,env --delay 900ms --exec "npm run appwrite-deploy"',
      'appwrite-deploy':
        'echo "Internal Error: Script was not overwritten!" && exit 1',
    },
    dependencies: {
      zod: '^3.22.4',
      '@written/app': '^0.2.1',
      'node-appwrite': '^11.0.0',
      dotenv: '^16.3.1',
      'source-map-support': '^0.5.21',
    },
    devDependencies: {
      esbuild: '^0.19.5',
      prettier: '^3.0.3',
    },
  },
  `# Logs
logs
*.log
npm-debug.log*
yarn-debug.log*
yarn-error.log*
lerna-debug.log*
.pnpm-debug.log*

# Diagnostic reports (https://nodejs.org/api/report.html)
report.[0-9]*.[0-9]*.[0-9]*.[0-9]*.json

# Runtime data
pids
*.pid
*.seed
*.pid.lock

# Directory for instrumented libs generated by jscoverage/JSCover
lib-cov

# Coverage directory used by tools like istanbul
coverage
*.lcov

# nyc test coverage
.nyc_output

# Grunt intermediate storage (https://gruntjs.com/creating-plugins#storing-task-files)
.grunt

# Bower dependency directory (https://bower.io/)
bower_components

# node-waf configuration
.lock-wscript

# Compiled binary addons (https://nodejs.org/api/addons.html)
build/Release

# Dependency directories
node_modules/
jspm_packages/

# Snowpack dependency directory (https://snowpack.dev/)
web_modules/

# TypeScript cache
*.tsbuildinfo

# Optional npm cache directory
.npm

# Optional eslint cache
.eslintcache

# Optional stylelint cache
.stylelintcache

# Microbundle cache
.rpt2_cache/
.rts2_cache_cjs/
.rts2_cache_es/
.rts2_cache_umd/

# Optional REPL history
.node_repl_history

# Output of 'npm pack'
*.tgz

# Yarn Integrity file
.yarn-integrity

# dotenv environment variable files
.env
.env.development.local
.env.test.local
.env.production.local
.env.local

# parcel-bundler cache (https://parceljs.org/)
.cache
.parcel-cache

# Next.js build output
.next
out

# Nuxt.js build / generate output
.nuxt
dist

# Gatsby files
.cache/
# Comment in the public line in if your project uses Gatsby and not Next.js
# https://nextjs.org/blog/next-9-1#public-directory-support
# public

# vuepress build output
.vuepress/dist

# vuepress v2.x temp and cache directory
.temp
.cache

# Docusaurus cache and generated files
.docusaurus

# Serverless directories
.serverless/

# FuseBox cache
.fusebox/

# DynamoDB Local files
.dynamodb/

# TernJS port file
.tern-port

# Stores VSCode versions used for testing VSCode extensions
.vscode-test

# yarn v2
.yarn/cache
.yarn/unplugged
.yarn/build-state.yml
.yarn/install-state.gz
.pnp.*`,
);
