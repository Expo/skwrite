import type * as http from 'http';

/** {@link http.IncomingMessage} */
export type AppwriteReq = {
  bodyRaw: string;
  /** JSON if contentType is application/json */
  body: any;
  /** Lowercase Key-Value Index of headers */
  headers: Record<string, string>;
  /** Method used */
  method: http.IncomingMessage['method'];
  /** Host, without port */
  host: string;
  /** The scheme used - http or https - no : */
  scheme: string;
  /** Query Parameters */
  query: Record<string, string>;
  /** Query String */
  queryString: string;
  /** The port - appears to be a string|number? Something feels off, someone needs to type openruntime */
  port: string | number;
  /** The URL the user appears to have used */
  url: string;
  /** The path */
  path: string;
};

export type AppwriteResponseObject = {
  body: any;
  statusCode: number;
  headers: Record<string, string>;
};

export type AppwriteRes = {
  /** Get a raw data response object */
  send: (
    body: any,
    statusCode?: number,
    headers?: Record<string, string>,
  ) => AppwriteResponseObject;
  /** Get a JSON data response object */
  json: (
    obj: Record<any, any>,
    statusCode?: number,
    headers?: Record<string, string>,
  ) => AppwriteResponseObject;
  /** Get an empty response object */
  empty: () => AppwriteResponseObject;
};

export type AppwriteDefault = (args: {
  /** Request Info */
  req: AppwriteReq;
  /** Response helpers */
  res: AppwriteRes;
  /** Log something! */
  log: (message: any) => void;
  /** Send an error message! */
  error: (message: any) => void;
}) => Promise<AppwriteResponseObject> | AppwriteResponseObject;

export default AppwriteDefault;
