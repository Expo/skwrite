const { execSync } = require('child_process');
const fs = require('fs');
const { writeFileSync, readFileSync } = fs;
const path = require('path');
const prompts = require('prompts');

const cmd = str =>
  execSync(str, {
    cwd: __dirname,
    stdio: 'inherit',
  });

if (!fs.existsSync(__dirname + '/packages/appwrite/.env'))
  fs.copyFileSync(
    __dirname + '/packages/appwrite/.env.example',
    __dirname + '/packages/appwrite/.env',
  );

/** @type {prompts.Options} */
const promptOpt = {
  onCancel: () => process.exit(1),
};

if (process.platform !== 'linux') throw new Error('only works on linux ok :D');

(async () => {
  console.clear();
  try {
    execSync('pnpm --version');
  } catch (error) {
    throw new Error(
      'PNPM is not installed. Please install it - https://pnpm.io/installation',
    );
  }
  cmd(`pnpm i;
pnpm i -g appwrite-cli;`);
  console.clear();
  console.log('\x1b[0;34m// General Setup //\x1b[0m');
  const pkg = JSON.parse(readFileSync('package.json', 'utf-8') || '{}');
  const { name } = await prompts(
    {
      name: 'name',
      type: 'text',
      min: 1,
      message: 'What is the name of your app?',
      initial: pkg.name ?? 'example-co',
      validate: v =>
        v.toLowerCase() === v
          ? v.match(/^[a-z0-9-]*$/giu)
            ? true
            : 'Must only contain a-z, 0-9 and hyphens.'
          : 'Must be lowercase!',
    },
    promptOpt,
  );
  pkg.name = name;
  writeFileSync('package.json', JSON.stringify(pkg, null, 2) + '\n');
  const svpkg = JSON.parse(
    readFileSync('packages/svelte/package.json', 'utf-8'),
  );
  svpkg.name = `${name}-svelte`;
  writeFileSync(
    'packages/svelte/package.json',
    JSON.stringify(svpkg, null, 2) + '\n',
  );
  const awpkg = JSON.parse(
    readFileSync('packages/appwrite/package.json', 'utf-8'),
  );
  awpkg.name = `${name}-appwrite`;
  writeFileSync(
    'packages/appwrite/package.json',
    JSON.stringify(awpkg, null, 2) + '\n',
  );
  const awj = JSON.parse(
    readFileSync('packages/appwrite/appwrite.json', 'utf-8'),
  );
  awj.projectId = name;
  awj.projectName = name;
  writeFileSync(
    'packages/appwrite/appwrite.json',
    JSON.stringify(awj, null, 2) + '\n',
  );
  console.log('\n\x1b[0;34m// SvelteKit Production Setup //\x1b[0m');
  const compose = readFileSync('packages/svelte/compose.yml', 'utf-8');
  const { port, loadBalance } = await prompts(
    [
      {
        name: 'port',
        type: 'number',
        min: 1,
        max: 65535,
        message: 'Which port do you want to locally expose svelte on?',
        initial: compose.match(/127\.0\.0\.1:([0-9]+)/)[1] ?? 3000,
      },
      {
        name: 'loadBalance',
        type: 'number',
        min: 1,
        max: 32,
        message: 'How many instances of svelte do you want to load balance?',
        initial: 1,
      },
    ],
    promptOpt,
  );
  const newCompose = `x-${name}-svelte-master: &x-${name}-svelte-master
  image: ${name}
  build:
    context: .
    dockerfile: Dockerfile
  networks:
    - ${name}_network

services:
  ${name}-svelte-nginx:
    image: nginx
    container_name: ${name}-svelte-nginx
    ports:
      - '127.0.0.1:${port}:80'
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf
    depends_on:${new Array(loadBalance)
      .fill(0)
      .map(
        (_, i) => `
      - ${name}-svelte-${i}`,
      )
      .join('')}
    networks:
      - ${name}_network
  # BEGIN_SVELTEKIT_INSTANCES${new Array(loadBalance)
    .fill(0)
    .map(
      (_, i) => `
  ${name}-svelte-${i}:
    container_name: ${name}-svelte-${i}
    <<: *x-${name}-svelte-master`,
    )
    .join('')}
  # END_SVELTEKIT_INSTANCES

networks:
  ${name}_network:
    driver: bridge
`;
  const newNginx = `events {
  worker_connections 1024;
}

http {
  upstream svelte_servers {
    # BEGIN_SVELTEKIT_INSTANCES
${new Array(loadBalance)
  .fill(0)
  .map((_, i) => `    server ${name}-svelte-${i}:3000;`)
  .join('\n')}
    # END_SVELTEKIT_INSTANCES
  }

  server {
    listen 80;
    server_name localhost;

    location / {
      proxy_pass http://svelte_servers;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "Upgrade";
      proxy_set_header Host $host;
    }
  }
}
`;
  writeFileSync('packages/svelte/compose.yml', newCompose);
  writeFileSync('packages/svelte/nginx.conf', newNginx);
  console.log('\n\x1b[0;34m// AppWrite Setup //\x1b[0m');
  try {
    execSync('docker logs appwrite > /dev/null 2>/dev/null');
    console.log('Appwrite is already installed! Skipping...');
  } catch (error) {
    console.log('Appwrite is not installed, installing...');
    const appwriteDir = path.resolve(
      (
        await prompts(
          {
            name: 'dir',
            type: 'text',
            initial: '/etc/appwrite',
            message: 'Where do you wish to store your global appwrite config?',
          },
          promptOpt,
        )
      ).dir,
    );
    cmd(`sudo echo -n ""
sudo mkdir -p ${JSON.stringify(appwriteDir)}
cd ${JSON.stringify(appwriteDir)}
sudo chown ${JSON.stringify(process.env.USER)} --recursive .
sudo docker run -it --rm \\
    --volume /var/run/docker.sock:/var/run/docker.sock \\
    --volume ${JSON.stringify(appwriteDir)}:/usr/src/code/appwrite:rw \\
    --entrypoint="install" \\
    appwrite/appwrite:1.4.9
sudo chown ${JSON.stringify(process.env.USER)} --recursive .`);
    if (
      !(
        await prompts(
          {
            name: 'isprod',
            type: 'confirm',
            initial: false,
            message: 'Is this a production environment?',
          },
          promptOpt,
        )
      ).isprod
    )
      writeFileSync(
        path.join(appwriteDir, '.env'),
        readFileSync(path.join(appwriteDir, '.env'), 'utf-8').replace(
          /_APP_ENV=production/,
          '_APP_ENV=development',
        ),
      );
    writeFileSync(
      path.join(appwriteDir, '.env'),
      readFileSync(path.join(appwriteDir, '.env'), 'utf-8').replace(
        /node-1(6|8)\.0/giu,
        'node-20.0',
      ),
    );
    const startScript = path.join(appwriteDir, 'start.sh');
    writeFileSync(
      startScript,
      `#!/bin/bash
  cd ${JSON.stringify(appwriteDir)}; docker compose up -d`,
    );
    execSync(`chmod +x ${JSON.stringify(startScript)}`);
    const stopScript = path.join(appwriteDir, 'stop.sh');
    writeFileSync(
      stopScript,
      `#!/bin/bash
  cd ${JSON.stringify(appwriteDir)}; docker compose down`,
    );
    execSync(`chmod +x ${JSON.stringify(stopScript)}`);
    console.log('Finished installing to ' + appwriteDir);
    console.log(
      `You may want to add \`@reboot ${JSON.stringify(
        startScript,
      )}\` to your cron file, to ensure appwrite's running on boot.`,
    );
  }
  console.log('\n\x1b[0;34m// Init Git //\x1b[0m');
  if (!fs.existsSync('.git'))
    cmd(`git init --initial-branch master
git add . || true
git commit -m "feat: Initial Commit" || true
`);
})();
