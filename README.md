# Skwrite

## Setup

To setup skwrite, run `setup.js` - it should setup a system-wide appwrite, aswell as initialize the repo.

Following that, run `/etc/appwrite/start.sh` to start appwrite. It may be worth adding this to a startup script/reboot cron job.

Once you're done with that, run `appwrite deploy collection --all` in `packages/appwrite`, to deploy the example database. Don't run this if your `db` database already has any data; it will get lost!

Finally, you'll need to modify the following files to point to your appwrite:

- [`/packages/svelte/src/lib/appwrite.ts`](packages/svelte/src/lib/appwrite.ts)
- [`/packages/appwrite/.env`](packages/appwrite/)

And you'll want to copy `/packages/appwrite/functions/Example Function/.env.example` to `/packages/appwrite/functions/Example Function/.env`

## Dev

Run `pnpm dev` to start a dev env.

## Prod

Simply run `docker compose up`. Make sure appwrite is setup; you can call `setup.js` on your server seperately if it isn't yet. Make sure it starts on system boot!
